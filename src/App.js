import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "components/Routes";

const App = () => {
  return (
    <Router>
      <div id="app">
        <Routes />
      </div>
    </Router>
  );
};

export default App;
