import React from "react";
import { Route, Switch } from "react-router-dom";
import Matches from "pages/matches/index";
import NotFound from "pages/NotFound";
import MatchEdit from "pages/matches/MatchEdit";

const Routes = () => {
  return (
    <Switch>
      <Route
        exact
        path="/"
        component={Matches}
      />
      <Route exact path="/matches/:id" component={MatchEdit}/>
      <Route component={NotFound} />
    </Switch>
  );
};

export default Routes;