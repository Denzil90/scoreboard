import React from "react";
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import App from "./App";

describe("App component", () => {
  configure({
    adapter: new Adapter(),
  });

  test("Matches the snapshot", () => {
    const appComponent = mount(<App />);
    expect(appComponent.find('#app').length).toBe(1);
  });
});
