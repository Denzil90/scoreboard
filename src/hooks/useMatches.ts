import { useEffect, useState } from "react";
import { Match } from "models/Match";
import MatchesService from "services/matches_service";

const useMatches = () => {
  const [matches, setMatches] = useState<Match[]>([]);

  useEffect(() => {
    MatchesService.loadMatches()
      .then((response) => setMatches(response))
      .catch((error) => console.log(error));
  }, []);

  return { matches, setMatches };
};

export default useMatches;
