import React, { useState } from "react";
import { Match } from "models/Match";
import MatchNewForm from "./MatchNewForm";

type MatchNewProps = {
  matches: Match[],
  setMatches: Function
}

const MatchNew = ({matches, setMatches}: MatchNewProps) => {
  const [visible, setVisible] = useState(false);
  return (
    <>
      <li style={{ textAlign: "center", marginTop: "20px" }}>
        <button onClick={() => setVisible(!visible)}>Create new match</button>
      </li>
      <MatchNewForm visible={visible} matches={matches} setMatches={setMatches} />
    </>
  );
};

export default MatchNew;
