import React from "react";
import { Match } from "models/Match";
import { Link } from "react-router-dom";

const MatchComponent = (match: Match) => {
  const { team1, team2, id } = match;
  return (
    <li className="container">
      <div className="col">
        {team1.teamName}
        <br />
        {team1.score}
      </div>
      <div className="col">
        {team2.teamName}
        <br />
        {team2.score}
      </div>
      <div>
        <Link to={`/matches/${id}`}>Edit</Link>
      </div>
    </li>
  );
};

export default MatchComponent;
