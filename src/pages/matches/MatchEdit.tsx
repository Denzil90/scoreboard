import React, { useEffect, useState } from "react";
import { Match } from "models/Match";
import { Link } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import useMatches from "hooks/useMatches";
import MatchesService from "services/matches_service";

const MatchEditSchema = Yup.object().shape({
  team1Score: Yup.number().required().min(0),
  team2Score: Yup.number().required().min(0),
});

const MatchEditComponent = (props: any) => {
  const { id } = props.match.params;
  const { matches } = useMatches();
  const [matchInfo, setMatchInfo] = useState<Match>();

  const handleSubmit = (values: any) => {
    /* istanbul ignore next */
    MatchesService.updateMatchScore(
      matches,
      parseInt(id),
      parseInt(values.team1Score),
      parseInt(values.team2Score)
    )
      .then((response) => {
        alert("you have updated the match");
      })
      .catch((error) => {
        alert("something went wrong");
      });
  };

  useEffect(() => {
    const match = matches.find((el) => {
      return el.id === parseInt(id);
    });
    if (match) {
      setMatchInfo(match);
    }
  }, [id, matches]);
  return (
    <Formik
      initialValues={{
        team1Score: matchInfo?.team1.score,
        team2Score: matchInfo?.team2.score,
      }}
      validationSchema={MatchEditSchema}
      onSubmit={(values, actions) => {
        handleSubmit(values);
      }}
      enableReinitialize
    >
      {({ isSubmitting, setFieldValue }) => (
        <div className="wrapper" style={{ marginTop: "10px" }}>
          <Form id="matchEdit">
            <div className="container">
              <div className="col">
                <label>{matchInfo?.team1.teamName}</label>
              </div>
              <div className="col">
                <Field name="team1Score" />
                <ErrorMessage component="small" name="team1Score" />
              </div>
            </div>
            <div className="container">
              <div className="col">
                <label>{matchInfo?.team2.teamName}</label>
              </div>
              <div className="col">
                <Field name="team2Score" />
                <ErrorMessage component="small" name="team2Score" />
              </div>
            </div>
            <div className="container" style={{marginTop: '10px'}}>
              <div className="col">
                <Link to="/">Back</Link>
              </div>
              <div className="col">
                <button type="submit">Send</button>
              </div>
            </div>
          </Form>
        </div>
      )}
    </Formik>
  );
};

export default MatchEditComponent;
