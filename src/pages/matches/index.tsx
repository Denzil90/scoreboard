import React from "react";
import useMatches from "hooks/useMatches";
import Match from "pages/matches/Match";
import MatchNew from "./MatchNew";

const MatchesComponent = () => {
  const { matches, setMatches } = useMatches();

  return (
    <div id="matchesWrapper">
      <h4 style={{ textAlign: "center" }}>Matches</h4>
      <ul>
        {matches.map((match) => (
          <Match
            key={match.id}
            id={match.id}
            team1={match.team1}
            team2={match.team2}
          />
        ))}
        <MatchNew matches={matches} setMatches={setMatches} />
      </ul>
    </div>
  );
};

export default MatchesComponent;
