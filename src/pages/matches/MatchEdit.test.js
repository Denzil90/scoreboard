import React from "react";
import Adapter from "enzyme-adapter-react-16";
import { createMemoryHistory } from "history";
import { mount, configure } from "enzyme";
import MatchEdit from "./MatchEdit";
import { Router } from "react-router-dom";

configure({ adapter: new Adapter() });
const history = createMemoryHistory();

const MatchEditWrapper = mount(
  <Router history={history}>
    <MatchEdit match={{ params: 1 }} />
  </Router>
);

test("it will show Match edition form", () => {
  expect(MatchEditWrapper.find("#matchEdit").length > 0).toBeTruthy();
});
