import React from "react";
import Adapter from "enzyme-adapter-react-16";
import { mount, configure } from "enzyme";
import Matches from "./index";

configure({ adapter: new Adapter() });

const MatchesWrapper = mount(<Matches />);

test("it will show all Matches", () => {
  expect(MatchesWrapper.find("#matchesWrapper").length > 0).toBeTruthy();
});
