import React from "react";
import Adapter from "enzyme-adapter-react-16";
import { createMemoryHistory } from "history";
import { mount, configure } from "enzyme";
import MatchNew from "./MatchNew";
import { Router } from "react-router-dom";

configure({ adapter: new Adapter() });
const history = createMemoryHistory();

const MatchEditWrapper = mount(
  <Router history={history}>
    <MatchNew />
  </Router>
);

test("it will show Match new form", () => {
  expect(MatchEditWrapper.find("#matchNew").length > 0).toBeTruthy();
});
