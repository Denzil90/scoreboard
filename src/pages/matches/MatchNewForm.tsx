import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Match } from "models/Match";
import MatchesService from "services/matches_service";

type MatchNewProp = {
  visible: boolean;
  matches: Match[];
  setMatches: Function;
};

const MatchNewSchema = Yup.object().shape({
  team1Name: Yup.string().required(),
  team2Name: Yup.string().required(),
});

const MatchNewForm = ({ visible, matches, setMatches }: MatchNewProp) => {
  const handleSubmit = (values: any) => {
    /* istanbul ignore next */
    MatchesService.startNewMatch(
      matches,
      { teamName: values.team1Name },
      { teamName: values.team2Name }
    )
      .then((response) => {
        alert("created");
        setMatches(response);
      })
      .catch((error) => {
        alert("something wrong creating new teams");
      });
  };

  if (visible) {
    return (
      <Formik
        initialValues={{
          team1Name: "",
          team2Name: "",
        }}
        validationSchema={MatchNewSchema}
        onSubmit={(values, actions) => {
          handleSubmit(values);
        }}
      >
        {({ isSubmitting, setFieldValue }) => (
          <Form id="matchNew" style={{marginTop: '10px'}}>
            <div className="container">
              <div className="col">
                <label>Team local name</label>
              </div>

              <div className="col">
                <Field name="team1Name" />
                <ErrorMessage component="small" name="team1Name" />
              </div>
            </div>
            <div className="container">
              <div className="col">
                <label>Team visitor name</label>
              </div>

              <div className="col">
                <Field name="team2Name" />
                <ErrorMessage component="small" name="team2Name" />
              </div>
            </div>
            <div className="container" style={{marginTop: '10px'}}>
              <div className="col"></div>
              <div className="col">
                <button type="submit">Send</button>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    );
  }
  return null;
};

export default MatchNewForm;
