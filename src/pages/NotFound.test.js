import React from "react";
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import NotFound from "./NotFound";

describe("Not found component", () => {
  configure({
    adapter: new Adapter(),
  });

  test("Shows the not found message", () => {
    const appComponent = mount(<NotFound />);
    expect(appComponent.find('h1').text()).toBe('Not found');
  });
});
