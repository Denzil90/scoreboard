export interface Team {
  teamName: string;
  score?: number;
}

export interface Match {
  id: number;
  team1: Team;
  team2: Team;
}