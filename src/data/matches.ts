const matches = [
  {
    id: 1,
    team1: {
      teamName: "Mexico",
      score: 0,
    },
    team2: {
      teamName: "Canada",
      score: 5,
    },
  },
  {
    id: 2,
    team1: {
      teamName: "Spain",
      score: 10,
    },
    team2: {
      teamName: "Brazil",
      score: 2,
    },
  },
  {
    id: 3,
    team1: {
      teamName: "Germany",
      score: 2,
    },
    team2: {
      teamName: "France",
      score: 2,
    },
  },
  {
    id: 4,
    team1: {
      teamName: "Uruguay",
      score: 6,
    },
    team2: {
      teamName: "Italy",
      score: 6,
    },
  },
  {
    id: 5,
    team1: {
      teamName: "Argentina",
      score: 3,
    },
    team2: {
      teamName: "Australia",
      score: 1,
    },
  },
];

export default matches;
