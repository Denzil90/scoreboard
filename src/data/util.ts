import { Match } from "../models/Match";
export const updateMatch = (
  store: Match[],
  id: number,
  teamScore1: number,
  teamScore2: number
) => {
  const copiedStore = [...store];
  const element = copiedStore.find((el) => el.id === id);
  if (element) {
    element.team1.score = teamScore1;
    element.team2.score = teamScore2;
  }
  return copiedStore;
};
