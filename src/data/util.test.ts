import { updateMatch } from "./util";

const matches = [
  {
    id: 1,
    team1: {
      teamName: "Mexico",
      score: 0,
    },
    team2: {
      teamName: "Canada",
      score: 5,
    },
  }
];

export default matches;


describe("Utils", () => {
  it("updates a match", () => {
    expect(matches[0].team1.score).toBe(0);
    expect(matches[0].team2.score).toBe(5);

    const modifiedMatches = updateMatch(matches, 1, 2, 7);
    expect(modifiedMatches[0].team1.score).toBe(2);
    expect(modifiedMatches[0].team2.score).toBe(7);
  });
});
