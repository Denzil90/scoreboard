import MatchesService from "./matches_service";

describe("Matches Service", () => {
  it("will load the matches", () => {
    MatchesService.loadMatches((response) => {
      expect(response).toHaveLength(5);
      done();
    });
  });

  it("will start new match", () => {
    MatchesService.startNewMatch(
      [],
      { teamName: "Russia" },
      { teamName: "Japan" },
      (response) => {
        expect(response).toHaveLength(1);
      }
    );
  });

  it("will update a match score", () => {
    MatchesService.updateMatchScore(
      [
        {
          id: 1,
          team1: { teamName: "Russia", score: 0 },
          team2: { teamName: "Japan", score: 5 },
        },
      ],
      1,
      2,
      7,
      (response) => {
        expect(response.team1.score).toBe(2);
        expect(response.team2.score).toBe(7);
      }
    );
  });

  it("will show an error when it doesn't find a record to update", () => {
    const promise = MatchesService.updateMatchScore(
      [
        {
          id: 1,
          team1: { teamName: "Russia", score: 0 },
          team2: { teamName: "Japan", score: 5 },
        },
      ],
      2,
      2,
      7
    ).catch(error => {
      expect(error.message).toEqual('Element not found')
    });
    promise.finally()
  })
});
