import { Match, Team } from "../models/Match";
import data from "../data/matches";

class MatchesService {
  static loadMatches = async () => Promise.resolve(data);

  static startNewMatch = async (data: Match[], team1: Team, team2: Team) => {
    const newData = [
      ...data,
      {
        id: data.length,
        team1: {
          teamName: team1.teamName,
          score: 0,
        },
        team2: {
          teamName: team2.teamName,
          score: 0,
        },
      },
    ];

    return Promise.resolve(newData);
  };

  static updateMatchScore = async (
    data: Match[],
    id: number,
    team1Score: number,
    team2Score: number
  ) => {
    const newData = [...data];
    let newElem = newData.find((el) => el.id === id);
    if (newElem) {
      newElem.team1.score = team1Score;
      newElem.team2.score = team2Score;
    } else {
      throw new Error("Element not found");
    }
    return Promise.resolve(newElem);
  };
}

export default MatchesService;
