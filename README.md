# README #

# prerequisites #

Need to have Node installed.

To install the project: npm install

To generate the minified version (over dist folder): npm run build

To run test: npm test coverage (this generates a folder coverage with iconv-report/index.html file with the coverage): npm run coverage

# assumptions #
If I had an API, I would intercept (for tests) the axios call and mock the data